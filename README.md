# Osiedle / Residential

[PL]
Finalna wersja projektu ocenionego na 15/15 punktów.

Uproszczone polecenie:


- Utwórz osiedle z gotowymi mieszkaniami i miejscami parkingowymi oraz wstępnie przydzielonymi mieszkańcami
- Stwórz między innymi możliwość: 
    - Wynajmu pomieszczeń
    - Meldunku i wymeldunku mieszkańców
    - Włożenia i wyjęcia przedmiotów i pojazdów do miejsca parkingowego
    - Generowania raportu z osiedla

Pełne polecenie:
https://gitlab.com/Buczeq/Osiedle/-/blob/master/src/resources/polecenie.pdf

[ENG]
Final version of project graded 15 out of 15 possible points.

Simplified command:

- Create residential with hard-coded flats and parking spaces, and pre-allocated residents 
- Create i.a. possibilities: 
    - Rent a flat
    - Check-in and check-out of residents
    - Putting in and out of articles and vehicles to the parking space
    - Generating a report from the residential

Full command:
https://gitlab.com/Buczeq/Osiedle/-/blob/master/src/resources/polecenie.pdf
