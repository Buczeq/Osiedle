package pl.buczeq;

import pl.buczeq.controller.Controller;
import pl.buczeq.exception.TooManyThingsException;
import pl.buczeq.model.person.Address;
import pl.buczeq.model.person.Person;
import pl.buczeq.model.residential.BlockOfFlats;
import pl.buczeq.model.residential.Flat;
import pl.buczeq.model.residential.ParkingSpace;
import pl.buczeq.model.residential.Residential;
import pl.buczeq.model.storable.Article;
import pl.buczeq.model.storable.Storable;
import pl.buczeq.model.storable.vehicle.*;
import pl.buczeq.service.Service;
import pl.buczeq.threads.RentalChecker;
import pl.buczeq.threads.TimeChanger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;

public class Main {
    private static final Properties prop = loadProperties();
    public static void main(String[] args) {
        new TimeChanger().run();

        //inicjacja osiedla
        ArrayList<Person> people = initPeople();
        ArrayList<Storable> storables = initStorables();
        ArrayList<Residential> residentials = initResidential(storables, people);


        Service service = new Service(residentials, storables, people);

        new RentalChecker(service).run();
        new Controller(service).commandsHandler();
    }

    public static Properties loadProperties() {

        try (InputStream input = new FileInputStream("././src/resources/properties/messagesColored.properties")){

            Properties prop = new Properties();

            prop.load(new InputStreamReader(input, StandardCharsets.UTF_8));
            return prop;
        } catch (IOException ex) {
            ex.getStackTrace();
        }
        return new Properties();
    }


    private static ArrayList<Residential> initResidential(ArrayList<Storable> storables, ArrayList<Person> people) {
        ArrayList<Residential> residentials = new ArrayList<>();
        Residential residential = new Residential();
        BlockOfFlats block = new BlockOfFlats();

        Flat f1 = new Flat(60, 20);
        Flat f2 = new Flat(50, 16);
        Flat f3 = new Flat(95, 30);
        Flat f4 = new Flat(7, 10, 2);
        Flat f5 = new Flat(10, 5, 3);

        f1.rent(people.get(0));
        f2.rent(people.get(1));
        f3.rent(people.get(2));

        block.addFlat(f1);
        block.addFlat(f2);
        block.addFlat(f3);
        block.addFlat(f4);
        block.addFlat(f5);
        residential.addBlockOfFlats(block);
        try {
            ParkingSpace p1 = new ParkingSpace(80, 20);
            p1.addStorable(storables.get(0));
            p1.addStorable(storables.get(1));
            p1.addStorable(storables.get(2));
            p1.addStorable(storables.get(3));
            residential.addParkingSpace(p1);
            p1.rent(people.get(0));

            ParkingSpace p2 = new ParkingSpace(86, 24);
            p2.addStorable(storables.get(4));
            p2.addStorable(storables.get(5));
            residential.addParkingSpace(p2);
            p2.rent(people.get(1));

            ParkingSpace p3 = new ParkingSpace(80, 20);
            p3.addStorable(storables.get(6));
            residential.addParkingSpace(p3);
            p3.rent(people.get(2));

            ParkingSpace p4 = new ParkingSpace(4, 5, 4);
            p4.addStorable(storables.get(7));
            residential.addParkingSpace(p4);
            p4.rent(people.get(2));
        } catch (TooManyThingsException ex) {
            System.out.println(prop.getProperty("residentialInitException"));
//            Printer.println("Błąd podczas inicjalizacji osiedla. " +
//                    "Próbowano dodać zbyt duże przedmioty do miejsc parkingowych", Color.RED_BOLD);
        }
        ParkingSpace p5 = new ParkingSpace(5, 4, 4);
        residential.addParkingSpace(p5);

        ParkingSpace p6 = new ParkingSpace(6, 5, 4);
        residential.addParkingSpace(p6);

        residentials.add(residential);
        return residentials;
    }

    private static ArrayList<Person> initPeople() {
        //using always same address
        ArrayList<Person> people = new ArrayList<>();
        Address address = new Address("Smutna 15", "07-900", "Warszawa", "Mazowieckie");

        people.add(new Person("Tomasz", "Buczyński", "01291604557", address));
        people.add(new Person("Jakub", "Wesołowski", "00211846314", address));
        people.add(new Person("Piotr", "Smól", "96081931133", address));
        people.add(new Person("Anna", "Kowalska", "01322357467", address));
        people.add(new Person("Halina", "Nowak", "95122782929", address));
        people.add(new Person("Paulina", "Komosa", "96100122988", address));

        return people;
    }

    private static ArrayList<Storable> initStorables() {
        ArrayList<Storable> storables = new ArrayList<>();
        storables.add(new Article(1, 1, "Karton z ubraniami"));
        storables.add(new Article(2, 1, "Karton z ubraniami"));
        storables.add(new Article(2, 3, "Łóżko"));
        storables.add(new Article(2, 1, "Głośniki"));
        storables.add(new Article(1, 1, "Telewizor"));

        storables.add(new Boat(2, 4, "Łódź turystyczna", 123, 60, 4));
        storables.add(new Amphibian(12, "Amfibia wojskowa", 200, 90));
        storables.add(new CityCar(3, 4, "Cinquecento", 60, false));
        //   /\ASSIGNED
        //
        //   \/AVAILABLE
        storables.add(new Article(1, 1, "Karton z książkami"));
        storables.add(new Article(1, 1, "Płyty muzyczne"));
        storables.add(new Article(5, 4, "Biurko"));
        storables.add(new Article(2, 2, "Szafka"));
        storables.add(new Article(1, 1, "Radio"));

        storables.add(new Boat(1, 6, "Kajak", 0, 5, 0));
        storables.add(new Amphibian(12, "Amfibia prywatna", 170, 70));
        storables.add(new CityCar(3, 4, "Mercedes", 160, true));
        storables.add(new Motorcycle(1, 2, "Ścigacz", 120, 1000));
        storables.add(new OffroadCar(3, 4, "Ford", 120, true));
        storables.add(new Motorcycle(1, 2, "Skuter", 30, 50));
        storables.add(new OffroadCar(3, 4, "Jeep", 250, true));

        return storables;

    }
}
