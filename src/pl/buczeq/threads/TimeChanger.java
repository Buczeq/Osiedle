package pl.buczeq.threads;

import java.time.LocalDate;
import java.util.Timer;
import java.util.TimerTask;

public class TimeChanger implements Runnable {
    private static LocalDate date;

    public TimeChanger() {
        date = LocalDate.now();
    }

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                moveTimeADay();
            }
        }, 0, 5000);

    }

    private void moveTimeADay(){
        date = date.plusDays(1);
    }

    public static LocalDate getDate() {
        return date;
    }
}
