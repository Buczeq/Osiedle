package pl.buczeq.threads;

import pl.buczeq.model.residential.Flat;
import pl.buczeq.model.residential.ParkingSpace;
import pl.buczeq.model.residential.Rentable;
import pl.buczeq.model.storable.vehicle.Vehicle;
import pl.buczeq.service.Service;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

public class RentalChecker implements Runnable{

    private final Service service;

    public RentalChecker(Service service) {
        this.service = service;
    }

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkRentals();
            }
        }, 0, 10000);

    }

    private void checkRentals(){
        List<Rentable> rentableList = service.getRentables().stream()
                .filter(r -> r.getPayingPerson() != null)
                .filter(r -> r.getEndOfTheLease().isBefore(TimeChanger.getDate()))
                .collect(Collectors.toList());
        for(Rentable rentable : rentableList){
                if(DAYS.between(rentable.getEndOfTheLease(), TimeChanger.getDate()) <= 30 && !rentable.isRentCancelled()) {
                    rentable.getPayingPerson().issueWarning(String.valueOf(rentable.getId()));
                }
                else{
                    if(rentable.getClass().equals(Flat.class)){
                        clearFlat((Flat) rentable);
                    }else{
                        clearParkingSpace((ParkingSpace) rentable);
                    }
                }
        }
    }

    private void clearFlat(Flat flat){
        flat.getInhabitants().clear();
        flat.getPayingPerson().getRentedObjects().remove(flat);

        flat.setPayingPerson(null);
    }

    private void clearParkingSpace(ParkingSpace parkingSpace){
            parkingSpace.getStorables().stream()
                    .filter(s -> s instanceof Vehicle)
                    .findFirst()
                    .ifPresentOrElse(v -> removeVehicleAndPayForRentForTwoMonths(parkingSpace, (Vehicle) v), () -> {
                        parkingSpace.getStorables().clear();
                        parkingSpace.setPayingPerson(null);
                    });

    }

    private void removeVehicleAndPayForRentForTwoMonths(ParkingSpace parkingSpace, Vehicle vehicle){
        parkingSpace.getStorables().remove(vehicle);
        parkingSpace.setEndOfTheLease(parkingSpace.getEndOfTheLease().plusMonths(2));
    }

}
