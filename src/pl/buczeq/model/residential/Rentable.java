package pl.buczeq.model.residential;

import pl.buczeq.model.person.Person;

import java.time.LocalDate;

public abstract class Rentable {

    private LocalDate startOfTheLease;
    private LocalDate endOfTheLease;
    private Person payingPerson;
    private final int id;
    private static int idCounter = 1;
    private final int usableArea;
    private final int size;
    private boolean rentCancelled = false;

    public Rentable(int a, int b, int h) {
        this.id = idCounter++;
        this.usableArea = a*b;
        this.size = a*b*h;
    }

    public Rentable(int size, int usableArea) {
        this.id = idCounter++;
        this.usableArea = usableArea;
        this.size = size;
    }

    public abstract boolean rent(Person person);

    public abstract void showRentable();

    public void setStartOfTheLease(LocalDate startOfTheLease) {
        this.startOfTheLease = startOfTheLease;
    }

    public LocalDate getEndOfTheLease() {
        return endOfTheLease;
    }

    public void setEndOfTheLease(LocalDate endOfTheLease) {
        this.endOfTheLease = endOfTheLease;
    }

    public Person getPayingPerson() {
        return payingPerson;
    }

    public void setPayingPerson(Person payingPerson) {
        this.payingPerson = payingPerson;
    }

    public int getId() {
        return id;
    }

    public int getUsableArea() {
        return usableArea;
    }

    public abstract String getRaportData();

    public boolean isRentCancelled() {
        return rentCancelled;
    }

    public void setRentCancelled(boolean rentCancelled) {
        this.rentCancelled = rentCancelled;
    }
}
