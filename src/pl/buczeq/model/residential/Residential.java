package pl.buczeq.model.residential;

import java.util.ArrayList;

public class Residential {
    private ArrayList<BlockOfFlats> blocksOfFlats;
    private ArrayList<ParkingSpace> parkingSpaces;

    public Residential(){
        this.blocksOfFlats = new ArrayList<>();
        this.parkingSpaces = new ArrayList<>();
    }

    public ArrayList<BlockOfFlats> getBlocksOfFlats() {
        return blocksOfFlats;
    }

    public ArrayList<ParkingSpace> getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(ArrayList<ParkingSpace> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public void addBlockOfFlats(BlockOfFlats blockOfFlats){
        this.blocksOfFlats.add(blockOfFlats);
    }
    public void removeBlockOfFlats(BlockOfFlats blockOfFlats){
        this.blocksOfFlats.remove(blockOfFlats);
    }

    public void addParkingSpace(ParkingSpace parkingSpace){
        this.parkingSpaces.add(parkingSpace);
    }
    public void removeParkingSpace(ParkingSpace parkingSpace){
        this.parkingSpaces.remove(parkingSpace);
    }
}
