package pl.buczeq.model.residential;

import java.util.ArrayList;

public class BlockOfFlats {
    private final ArrayList<Flat> flats;

    public BlockOfFlats() {
        this.flats = new ArrayList<>();
    }

    public ArrayList<Flat> getFlats() {
        return flats;
    }

    public void addFlat(Flat flat){
        flats.add(flat);
    }
}
