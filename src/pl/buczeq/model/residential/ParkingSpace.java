package pl.buczeq.model.residential;

import pl.buczeq.controller.Controller;
import pl.buczeq.exception.TooManyThingsException;
import pl.buczeq.model.person.Person;
import pl.buczeq.model.storable.Storable;
import pl.buczeq.threads.TimeChanger;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

public class ParkingSpace extends Rentable {

    private final ArrayList<Storable> storables;
    private int remainingSpace;


    public ParkingSpace(int a, int b, int h) {
        super(a, b, h);
        this.storables = new ArrayList<>();
        this.remainingSpace = getUsableArea();
    }

    public ParkingSpace(int size, int usableArea) {
        super(size, usableArea);
        this.storables = new ArrayList<>();
        this.remainingSpace = getUsableArea();
    }


    public ArrayList<Storable> getStorables() {
        return storables;
    }

    public void addStorable(Storable storable) throws TooManyThingsException {
        if (remainingSpace - storable.getSurfaceArea() >= 0) {
            remainingSpace = remainingSpace - storable.getSurfaceArea();
            storable.setStored(true);
            storables.add(storable);
        } else {
            throw new TooManyThingsException("Remove some old items to insert a new item");
        }

    }

    public void removeStorable(Storable storable) {
        remainingSpace += storable.getSurfaceArea();
        storable.setStored(false);
        storables.remove(storable);
    }

    @Override
    public boolean rent(Person person) {
        if (getPayingPerson() == null) {
            if (person.getRentedObjects().isEmpty()) {
                Controller.cannotRentParkingBecauseNoFlatIsRented();
                return false;
            } else {
                setPayingPerson(person);
                person.addRentable(this);
                LocalDate now = TimeChanger.getDate();
                this.setStartOfTheLease(now);
                this.setEndOfTheLease(now.plusMonths(1));
                return true;
            }
        } else {

            Controller.rentableIsAlreadyRented();
            return false;
        }
    }

    @Override
    public void showRentable() {
        System.out.println(getId() + ". Miejsce parkingowe o kubaturze " + this.getUsableArea() + "m³");
    }


    @Override
    public String toString() {
        return getId() + ". Miejsce parkingowe o kubaturze " + this.getUsableArea() + "m³";
    }

    @Override
    public String getRaportData() {
        return MessageFormat.format("\n    {0}. Miejsce parkingowe o powierzchni {1}m²{2}{3}",
                getId(), getUsableArea(),
                getPayingPerson() != null
                        ? "\n        Osoba wynajmująca: " + getPayingPerson().getAllDataForRaport()
                        : "\n        Miejsce parkingowe nie jest wynajmowane",
                (storables.size() > 0)
                        ? ("        Przedmioty:" + getStorablesRaportData() + "\n\n")
                        : "\n\n");
    }

    private String getStorablesRaportData() {
        StringBuilder builder = new StringBuilder();
        storables.stream()
                .sorted(Comparator.comparing(Storable::getSurfaceArea)
                        .reversed()
                        .thenComparing(Storable::getName))
                .forEach(s -> builder.append("\n            ")
                        .append(s.getInfo()
                                .replace("\n", ", ")
                                .replace("    ", "")));

        return builder.toString();
    }


}
