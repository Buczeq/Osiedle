package pl.buczeq.model.residential;

import pl.buczeq.controller.Controller;
import pl.buczeq.model.person.Person;
import pl.buczeq.threads.TimeChanger;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;

public class Flat extends Rentable {

    private final ArrayList<Person> inhabitants;

    public Flat(int a, int b, int h) {
        super(a, b, h);
        this.inhabitants = new ArrayList<>();
    }

    public Flat(int size, int usableArea) {
        super(size, usableArea);
        this.inhabitants = new ArrayList<>();
    }

    @Override
    public boolean rent(Person person) {
        if (getPayingPerson() == null) {
            setPayingPerson(person);
            person.addRentable(this);
            inhabitants.add(person);
            LocalDate now = TimeChanger.getDate();
            this.setStartOfTheLease(now);
            this.setEndOfTheLease(now.plusMonths(1));
            return true;
        } else {
            Controller.rentableIsAlreadyRented();
            return false;
        }
    }

    public ArrayList<Person> getInhabitants() {
        return inhabitants;
    }

    @Override
    public void showRentable() {
        System.out.println(getId() + ". Mieszkanie o powierzchni " + this.getUsableArea() + "m²");
    }

    @Override
    public String toString() {
        return getId() + ". Mieszkanie o powierzchni " + this.getUsableArea() + "m²";
    }

    @Override
    public String getRaportData() {
        return MessageFormat.format("\n    {0}. Mieszkanie o powierzchni {1}m²{2}{3}",
                getId(), getUsableArea(),
                getPayingPerson() != null
                        ? "\n        Osoba wynajmująca: " + getPayingPerson().getAllDataForRaport()
                        : "\n        Mieszkanie nie jest wynajmowane",
                (inhabitants.size() > 1)
                        ? ("\n        Pozostali lokatorzy:\n" + getInhabitantsRaportData() + "\n")
                        : "\n");
    }

    private String getInhabitantsRaportData() {
        ArrayList<Person> inhabitantsWithoutRentier = this.inhabitants;
        inhabitantsWithoutRentier.remove(getPayingPerson());
        StringBuilder builder = new StringBuilder();
        inhabitantsWithoutRentier.forEach(i -> builder
                .append("                ")
                .append(i.getAllDataForRaport())
                .append("\n"));
        return builder.toString();
    }

}
