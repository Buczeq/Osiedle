package pl.buczeq.model.person;

public class Address {
    private final String street;
    private final String postalCode;
    private final String city;
    private final String province;

    public Address(String street, String postalCode, String city, String province) {
        this.street = street;
        this.postalCode = postalCode;
        this.city = city;
        this.province = province;
    }

    @Override
    public String toString() {
        return street + " " + city + " " + province + " " + postalCode;
    }
}
