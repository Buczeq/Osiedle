package pl.buczeq.model.person;

import pl.buczeq.model.residential.Rentable;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

public class Person {
    private final int id;
    private static int idCounter = 1;
    private final String firstname;
    private final String lastname;
    private final String pesel;
    private final Address address;
    private final LocalDate birthDate;

    private final ArrayList<Rentable> rentedObjects;

    private final ArrayList<File> warnings;

    public Person(String firstname, String lastname, String pesel, Address address) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.address = address;
        this.birthDate = generateBirthDateFromPesel(pesel);
        this.id = idCounter++;
        this.rentedObjects = new ArrayList<>();
        this.warnings = new ArrayList<>();
    }

    public void issueWarning(String rentableId){
        if(warnings.stream().noneMatch(w -> w.getName().equals(rentableId))){
            warnings.add(new File(rentableId));
        }

    }

    public void addRentable(Rentable rentable){
        rentedObjects.add(rentable);
    }

    public String getNameAndPesel(){
        return "%s %s %s"
                .formatted(getFirstname(), getLastname(), getPesel());
    }

    public String getAllData(){
        return ("""
                %d. %s %s
                PESEL: %s
                Data urodzenia: %s
                Adres: %s
                Wynajęte pomieszczenia:\s
                %s
                Zadłużenia:\s
                %s""")
                .formatted(getId(), getFirstname(), getLastname(), getPesel(), getBirthDate(), getAddress(), getRentedObjects(), getWarningsInfo());
    }

    public String getAllDataForRaport(){
        return ("""

                            %d. %s %s
                            PESEL: %s
                            Data urodzenia: %s
                            Adres: %s
                            Zadłużenia:%s
                """)
                .formatted(getId(), getFirstname(), getLastname(), getPesel(), getBirthDate(), getAddress(), getWarningsInfoForRaport());
    }

    private String getWarningsInfoForRaport(){
        StringBuilder builder = new StringBuilder();
        if(this.getWarnings().isEmpty()){
            builder.append(" Brak nieopłaconych pomieszczeń.");
        }else {
            for (File file : this.getWarnings()) {
                builder.append("\n                Nieopłacone pomieszczenie o id: ");
                builder.append(file.getName());
            }
        }
        return builder.toString();
    }

    private String getWarningsInfo(){
        StringBuilder builder = new StringBuilder();
        if(this.getWarnings().isEmpty()){
            builder.append("Brak nieopłaconych pomieszczeń.");
        }else {
            for (File file : this.getWarnings()) {
                builder.append("Nieopłacone pomieszczenie o id: ");
                builder.append(file.getName());
                builder.append("\n");
            }
        }
        return builder.toString();
    }
    private LocalDate generateBirthDateFromPesel(String pesel){
        int[] digits = new int[pesel.length()];
        for (int i = 0; i < pesel.length(); i++){
            digits[i] = pesel.charAt(i) - '0';
        }

        int year = digits[0]*10 + digits[1];
        switch (digits[2]){
            case 0, 1 -> year += 1900;
            case 2, 3 -> year += 2000;
            case 4, 5 -> year += 2100;
            case 6, 7 -> year += 2200;
            case 8, 9 -> year += 1800;
        }

        int month;
        if(digits[2] % 2 == 0){
            month = digits[3];
        }else{
            month = digits[3] + 10;
        }

        int day = digits[4]*10 + digits[5];

        return LocalDate.of(year, month, day);

    }

    public int getId() {
        return id;
    }

    public ArrayList<Rentable> getRentedObjects() {
        return rentedObjects;
    }

    public String getPesel() {
        return pesel;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public ArrayList<File> getWarnings() {
        return warnings;
    }
}
