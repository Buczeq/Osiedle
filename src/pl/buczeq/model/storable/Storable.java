package pl.buczeq.model.storable;

public abstract class Storable {

    private final String name;
    private final int surfaceArea;
    private final int id;
    private static int idCounter = 1;
    private boolean isStored;

    public String getInfo(){
        return getId() + ". Rozmiar: " + getSurfaceArea() + "m²\n" + getName();
    };

    public Storable(int a, int b, String name){
        this.name = name;
        this.surfaceArea = a*b;
        this.id = idCounter++;
        this.isStored = false;
    }
    public Storable( int surfaceArea, String name){
        this.name = name;
        this.surfaceArea = surfaceArea;
        this.id = idCounter++;
        this.isStored = false;
    }

    public String getName() {
        return name;
    }

    public int getSurfaceArea() {
        return surfaceArea;
    }

    public int getId() {
        return id;
    }

    public boolean isStored() {
        return isStored;
    }

    public void setStored(boolean stored) {
        isStored = stored;
    }

    @Override
    public abstract String toString();
}
