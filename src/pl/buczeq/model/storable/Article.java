package pl.buczeq.model.storable;

public class Article extends Storable{

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    public Article(int a, int b, String name) {
        super(a, b, name);
    }

    public Article(int surfaceArea, String name) {
        super(surfaceArea, name);
    }

    @Override
    public String toString() {
        return getId() + ". " + getName();
    }
}
