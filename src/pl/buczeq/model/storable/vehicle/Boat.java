package pl.buczeq.model.storable.vehicle;

public class Boat extends Vehicle{

    private final int swimmingSpeed;
    private final int amountOfMasts;


    public Boat(int a, int b, String name, int horsePower, int swimmingSpeed, int amountOfMasts) {
        super(a, b, name, horsePower);
        this.swimmingSpeed = swimmingSpeed;
        this.amountOfMasts = amountOfMasts;
    }

    public Boat( int surfaceArea, String name, int horsePower, int swimmingSpeed, int amountOfMasts) {
        super(surfaceArea, name, horsePower);
        this.swimmingSpeed = swimmingSpeed;
        this.amountOfMasts = amountOfMasts;
    }

    public int getSwimmingSpeed() {
        return swimmingSpeed;
    }

    public int getAmountOfMasts() {
        return amountOfMasts;
    }


    @Override
    public String getInfo() {
        return super.getInfo() + "\n    Prędkość pływania: " + getSwimmingSpeed() + "km/h\n    Ilość masztów: " + getAmountOfMasts();
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nPrędkość pływania: " +
                getSwimmingSpeed() +
                "km/h\nIlość masztów: " +
                getAmountOfMasts();
    }
}
