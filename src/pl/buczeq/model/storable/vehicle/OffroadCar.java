package pl.buczeq.model.storable.vehicle;

public class OffroadCar extends Vehicle{

    private final boolean fourWheelDrive;

    public OffroadCar(int a, int b, String name, int horsePower, boolean fourWheelDrive) {
        super(a, b, name, horsePower);
        this.fourWheelDrive = fourWheelDrive;
    }

    public OffroadCar(int surfaceArea, String name, int horsePower, boolean fourWheelDrive) {
        super(surfaceArea, name, horsePower);
        this.fourWheelDrive = fourWheelDrive;
    }

    public boolean isFourWheelDrive() {
        return fourWheelDrive;
    }


    @Override
    public String getInfo() {
        return super.getInfo() + "\n    " + (isFourWheelDrive() ? "Napęd na 4 koła" : " Brak napędu na 4 koła");
    }


    @Override
    public String toString() {
        return super.toString() + (isFourWheelDrive() ? "\nNapęd na 4 koła" : "\nBrak napędu na 4 koła");
    }
}
