package pl.buczeq.model.storable.vehicle;

public class CityCar extends Vehicle{

    private final boolean automaticTransmission;

    public CityCar(int a, int b, String name, int horsePower, boolean automaticTransmission) {
        super(a, b, name, horsePower);
        this.automaticTransmission = automaticTransmission;
    }

    public CityCar(int surfaceArea, String name, int horsePower, boolean automaticTransmission) {
        super(surfaceArea, name, horsePower);
        this.automaticTransmission = automaticTransmission;
    }

    public boolean isAutomaticTransmission() {
        return automaticTransmission;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + "\n    " + (isAutomaticTransmission() ? "Automatyczna skrzynia biegów" : "Brak automatycznej skrzyni biegów");
    }

    @Override
    public String toString() {
        return super.toString()  +
                (isAutomaticTransmission() ?
                        "\nAutomatyczna skrzynia biegów" :
                        "\nBrak automatycznej skrzyni biegów");
    }
}
