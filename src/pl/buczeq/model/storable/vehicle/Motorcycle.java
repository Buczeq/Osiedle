package pl.buczeq.model.storable.vehicle;

public class Motorcycle extends Vehicle{
    private final int engineCapacity;

    public Motorcycle(int a, int b, String name, int horsePower, int engineCapacity) {
        super(a, b, name, horsePower);
        this.engineCapacity = engineCapacity;
    }

    public Motorcycle(int surfaceArea, String name, int horsePower, int engineCapacity) {
        super(surfaceArea, name, horsePower);
        this.engineCapacity = engineCapacity;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + "\n    " + "Pojemność silinka: " + getEngineCapacity();
    }

    @Override
    public String toString() {
        return super.toString() + "\nPojemność silinka: " + getEngineCapacity();
    }
}
