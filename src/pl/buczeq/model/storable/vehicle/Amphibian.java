package pl.buczeq.model.storable.vehicle;


public class Amphibian extends Vehicle {

    private final int swimmingSpeed;

    public Amphibian(int a, int b, String name, int horsePower, int swimmingSpeed) {
        super(a, b, name, horsePower);
        this.swimmingSpeed = swimmingSpeed;
    }

    public Amphibian(int surfaceArea, String name, int horsePower, int swimmingSpeed) {
        super(surfaceArea, name, horsePower);
        this.swimmingSpeed = swimmingSpeed;

    }

    public int getSwimmingSpeed() {
        return swimmingSpeed;
    }


    @Override
    public String getInfo() {
        return super.getInfo() + "\n    Prędkość pływania: " + getSwimmingSpeed() + "km/h";
    }

    @Override
    public String toString() {
        return super.toString() + "\nPrędkość pływania: " + getSwimmingSpeed() + "km/h";
    }
}
