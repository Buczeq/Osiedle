package pl.buczeq.exception;

public class TooManyThingsException extends Exception{
    public TooManyThingsException(String message) {
        super(message);
    }
}
