package pl.buczeq.controller;

import pl.buczeq.Main;
import pl.buczeq.model.person.Person;
import pl.buczeq.model.residential.Flat;
import pl.buczeq.model.residential.ParkingSpace;
import pl.buczeq.model.residential.Rentable;
import pl.buczeq.model.storable.Storable;
import pl.buczeq.service.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Controller {
    private final Scanner scanner;
    private final Service service;
    private static final Properties prop = Main.loadProperties();

    public Controller(Service service) {
        this.scanner = new Scanner(System.in);
        this.service = service;
    }

    public void commandsHandler() {
        System.out.println(prop.getProperty("whatToDo"));

        try {
            switch (scanner.nextLine()) {
                case "pomoc" -> help();
                case "stop" -> stop();
                case "osoby" -> showPeople();
                case "osoba" -> selectPerson();
                case "pomieszczenia" -> showMyOrVacantRentables();
                case "pomieszczenie" -> getSelectedRentable();
                case "data" -> showDate();
                case "wynajmij" -> rentAttempt();
                case "anuluj wynajem" -> cancelRent();
                case "opłać" -> renewRent();
                case "dane" -> showMyData();
                case "generuj raport" -> generateRaport();
                default -> incorrect();
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            incorrect();
        }
        commandsHandler();
    }

    private void showDate() {
        service.showDate();
    }

    private void getSelectedRentable() {
        System.out.println(prop.getProperty("selectedRentable"));
        service.getSelectedRentable().ifPresentOrElse(Rentable::showRentable, this::rentableNotSelected);
    }

    private void help() {
        System.out.println(prop.getProperty("availableOptions"));
    }

    private void stop() {
        System.out.println(prop.getProperty("stopApp"));
        System.exit(1);
    }

    private void showPeople() {
        System.out.println(prop.getProperty("availablePeople"));
        service.getPeople().forEach(this::showPerson);
    }

    private void selectPerson() {
        System.out.println(prop.getProperty("selectPersonById"));
        try {
            service.selectPersonById(Integer.parseInt(scanner.nextLine()))
                    .ifPresentOrElse(this::showPerson,
                    () -> {
                        if (incorrectWantToTryAgain()) selectPerson();
                    });
        } catch (InputMismatchException | NumberFormatException ex) {
            if (incorrectWantToTryAgain()) selectPerson();
        }
    }

    private void showMyRentables() {
        if (service.getSelectedPersonRentables().isEmpty()) {
            System.out.println(prop.getProperty("youDontRentAnyRentables"));
        } else {
            System.out.println(prop.getProperty("yourRentables"));
            service.getSelectedPersonRentables().forEach(Rentable::showRentable);

            selectMyRentable(service.getSelectedPersonRentables());
        }
    }

    private void selectMyRentable(ArrayList<Rentable> myRentables) {
        selectRentable();
        Optional<Rentable> rentable = service.getSelectedRentable();
        if (rentable.isPresent()) {
            if (myRentables.contains(rentable.get())) {
                managaSelectedRentable(rentable.get());
            } else {
                System.out.println(prop.getProperty("selectedRentableIsNoRentedByYou"));
                if (incorrectWantToTryAgain()) selectMyRentable(myRentables);
            }

        }
    }

    private void managaSelectedRentable(Rentable rentable) {
        if (rentable.getClass().equals(Flat.class)) {
            manageInhabitants((Flat) rentable);
        } else {
            manageStorables((ParkingSpace) rentable);
        }
    }

    private void manageStorables(ParkingSpace parkingSpace) {
        System.out.println(prop.getProperty("whatToDoWithStorables"));
        System.out.println(prop.getProperty("manageStorablesCommands"));
        try {
            switch (scanner.nextLine()) {
                case "usuń" -> removeStorable(parkingSpace);
                case "dodaj" -> addStorable(parkingSpace);
                case "pokaż" -> seeStorables(parkingSpace);
                case "anuluj" -> {
                    return;
                }
                default -> {
                    if (incorrectWantToTryAgain()) manageStorables(parkingSpace);
                }
            }
        } catch (InputMismatchException ex) {
            if (incorrectWantToTryAgain()) manageStorables(parkingSpace);
        }
        manageStorables(parkingSpace);
    }


    public void seeInhabitants(Flat flat) {
        if (!flat.getInhabitants().isEmpty()) {
            System.out.println(prop.getProperty("inhabitantsInFlat"));
            flat.getInhabitants().forEach(this::showPerson);
        } else {
            System.out.println(prop.getProperty("inhabitantsNotFound"));
        }
    }

    public void seeStorables(ParkingSpace parkingSpace) {
        if (!parkingSpace.getStorables().isEmpty()) {
            System.out.println(prop.getProperty("storablesInRentable"));
            parkingSpace.getStorables().forEach(this::showStorable);
        } else {
            System.out.println(prop.getProperty("storablesNotFound"));
        }
    }

    private void manageInhabitants(Flat flat) {
        System.out.println(prop.getProperty("whatToDoWithInhabitants"));
        System.out.println(prop.getProperty("manageInhabitantsCommands"));
        try {
            switch (scanner.nextLine()) {
                case "usuń" -> removeInhabitant(flat);
                case "dodaj" -> addInhabitant(flat);
                case "pokaż" -> seeInhabitants(flat);
                case "anuluj" -> {
                    return;
                }
                default -> {
                    if (incorrectWantToTryAgain()) manageInhabitants(flat);
                    else return;
                }

            }
        } catch (InputMismatchException ex) {
            if (incorrectWantToTryAgain()) manageInhabitants(flat);
        }
        manageInhabitants(flat);
    }

    private void addInhabitant(Flat flat) {
        ArrayList<Person> inhabitants = flat.getInhabitants();
        ArrayList<Person> notInhabitants = service.getPeople().stream()
                .filter(p -> !inhabitants.contains(p))
                .collect(Collectors.toCollection(ArrayList::new));

        notInhabitants.forEach(this::showPerson);
        System.out.println(prop.getProperty("addInhabitantById"));
        try {
            int id = Integer.parseInt(scanner.nextLine());

            notInhabitants.stream()
                    .filter(i -> i.getId() == id)
                    .findFirst()
                    .ifPresentOrElse(p ->service.addInhabitant(p, flat),
                            () -> {
                                if (incorrectWantToTryAgain()) addInhabitant(flat);
                            });
        } catch (InputMismatchException | NumberFormatException ex) {
            if (incorrectWantToTryAgain()) addInhabitant(flat);
        }
    }

    private void addStorable(ParkingSpace parkingSpace) {
        service.getAvailableStorables().forEach(this::showStorable);
        System.out.println(prop.getProperty("addStorableById"));
        try {
            int id = Integer.parseInt(scanner.nextLine());
            service.getAvailableStorables().stream()
                    .filter(storable -> storable.getId() == id)
                    .findFirst()
                    .ifPresentOrElse(s -> service.addStorable(s, parkingSpace),
                    () -> {
                        if (incorrectWantToTryAgain()) addStorable(parkingSpace);
                    });
        } catch (InputMismatchException | NumberFormatException ex) {
            if (incorrectWantToTryAgain()) addStorable(parkingSpace);
        }

    }

    private void removeInhabitant(Flat flat) {
        ArrayList<Person> inhabitants = flat.getInhabitants();
        if(service.getSelectedPerson().isPresent()) {
            inhabitants.remove(service.getSelectedPerson().get());
        }
        if (!inhabitants.isEmpty()) {
            seeInhabitants(flat);
            System.out.println(prop.getProperty("removeInhabitantById"));
            try {
                int id = Integer.parseInt(scanner.nextLine());
                inhabitants.stream()
                        .filter(i -> i.getId() == id)
                        .findFirst()
                        .ifPresentOrElse(i -> {
                            service.removeInhabitant(i, flat);
                            operationWasSuccessfull();
                        }, () -> {
                            System.out.println(prop.getProperty("selectedInhabitantDoesntLiveHere"));
                            if (incorrectWantToTryAgain()) removeInhabitant(flat);
                        });

            } catch (InputMismatchException | NumberFormatException ex) {
                if (incorrectWantToTryAgain()) removeInhabitant(flat);
            }
        } else {
            System.out.println(prop.getProperty("thereIsNoOtherInhabitants"));
        }
    }

    private void removeStorable(ParkingSpace parkingSpace) {
        seeStorables(parkingSpace);
        System.out.println(prop.getProperty("removeStorableById"));
        try {
            int id = Integer.parseInt(scanner.nextLine());
            parkingSpace.getStorables().stream()
                    .filter(s -> s.getId() == id)
                    .findFirst()
                    .ifPresentOrElse(s -> {
                                parkingSpace.removeStorable(s);
                                operationWasSuccessfull();
                            },
                            () -> {
                                System.out.println(prop.getProperty("selectedStorableIsNotInThisRentable"));
                                if (incorrectWantToTryAgain()) removeStorable(parkingSpace);
                            }
                    );
        } catch (InputMismatchException | NumberFormatException ex) {
            if (incorrectWantToTryAgain()) removeStorable(parkingSpace);
        }

    }

    private boolean incorrectWantToTryAgain() {
        System.out.println(prop.getProperty("somethingsWrongWantToTryAgain"));
        return scanner.nextLine().toLowerCase()
                .matches("tak|yes|t|y");
    }

    private void showPerson(Person person) {
        System.out.println(person.getId() + ". " + person.getNameAndPesel());
    }

    private void showStorable(Storable storable) {
        System.out.println(storable.getId() + ". " + storable.getInfo());
    }

    private void showVacantRentables() {
        System.out.println(prop.getProperty("availableRentables"));
        service.getVacantRentables().forEach(Rentable::showRentable);
        selectRentable();
    }

    private void showMyOrVacantRentables() {
        System.out.println(prop.getProperty("whichRentablesYouWantToSee"));
        System.out.println(prop.getProperty("availableMineCancel"));
        try {
            switch (scanner.nextLine()) {
                case "wolne" -> showVacantRentables();
                case "moje" -> showMyRentables();
                case "anuluj" -> {
                }
                default -> {
                    if (incorrectWantToTryAgain()) showMyOrVacantRentables();
                }
            }
        } catch (InputMismatchException ex) {
            if (incorrectWantToTryAgain()) showMyOrVacantRentables();
        }
    }

    private void selectRentable() {
        System.out.println(prop.getProperty("selectRentableById"));
        try {
            int id = Integer.parseInt(scanner.nextLine());
            service.selectRentableById(id)
                    .ifPresentOrElse(r -> {
                                System.out.println(prop.getProperty("rentableSelected"));
                        r.showRentable();
                    },
                    () -> {
                        if (incorrectWantToTryAgain()) selectRentable();
                    });
        } catch (InputMismatchException | NumberFormatException ex) {
            if (incorrectWantToTryAgain()) selectRentable();
        }
    }

    public void rentAttempt() {
            service.rent();
    }

    public void cancelRent(){
        service.cancelRent();
    }

    private void renewRent() {
        service.renewRent();
    }

    private void generateRaport() {
        System.out.println(prop.getProperty("generatingRaport"));
        try {
            service.generateRaport();
        } catch (IOException ex) {
            System.out.println(prop.getProperty("generatingRaportFailed"));
        }
    }

    public static void personOrRentableNotSelected() {
        System.out.println(prop.getProperty("personOrRentableNotSelected"));
    }

    public static void cannotRentParkingBecauseNoFlatIsRented() {
        System.out.println(prop.getProperty("cannotRentParkingBecauseNoFlatIsRented"));
    }

    public static void rentableIsAlreadyRented() {

        System.out.println(prop.getProperty("rentableIsAlreadyRented"));
    }

    public static void rentableSuccessfullyRented() {
        System.out.println(prop.getProperty("rentableSuccessfullyRented"));
    }

    public static void alreadyRentedAllPossibleRentables() {
        System.out.println(prop.getProperty("alreadyRentedAllPossibleRentables"));

    }

    public static void youDontRentThisRentable() {
        System.out.println(prop.getProperty("youDontRentThisRentable"));
    }

    private void showMyData() {
        service.getSelectedPerson()
                .ifPresentOrElse(p -> System.out.println(prop.getProperty("yourData") + p.getAllData()),
                        this::personNotSelected);
    }

    private void personNotSelected() {
        System.out.println(prop.getProperty("personNotSelected"));
    }

    private void rentableNotSelected() {
        System.out.println(prop.getProperty("rentableNotSelected"));
    }

    public static void operationWasSuccessfull() {
        System.out.println(prop.getProperty("operationWasSuccessfull"));
    }

    public static void noMoreSpaceInParkingSpace(String message) {
        System.out.println(prop.getProperty("RED") + message);
    }

    public static void rentWillBeCancelled(){
        System.out.println(prop.getProperty("rentWillBeCancelled"));
    }

    private void incorrect() {
        System.out.println(prop.getProperty("incorrect"));
    }
}
