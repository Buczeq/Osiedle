package pl.buczeq.service;

import pl.buczeq.Main;
import pl.buczeq.controller.Controller;
import pl.buczeq.exception.ProblematicTenantException;
import pl.buczeq.exception.TooManyThingsException;
import pl.buczeq.model.person.Person;
import pl.buczeq.model.residential.Flat;
import pl.buczeq.model.residential.ParkingSpace;
import pl.buczeq.model.residential.Rentable;
import pl.buczeq.model.residential.Residential;
import pl.buczeq.model.storable.Storable;
import pl.buczeq.threads.TimeChanger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;


public class Service {

    private Optional<Person> selectedPerson;
    private Optional<Rentable> selectedRentable;
    private final ArrayList<Rentable> rentables;
    private final ArrayList<Person> people;
    private final ArrayList<Storable> storables;


    public Service(ArrayList<Residential> residentials, ArrayList<Storable> storables,ArrayList<Person> people) {
        this.selectedPerson = Optional.empty();
        this.selectedRentable = Optional.empty();
        this.people = people;
        this.storables = storables;
        this.rentables = getRentablesFromResidentials(residentials);
    }

    public void showDate() {
        System.out.println(TimeChanger.getDate().toString());
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public Optional<Person> selectPersonById(int id) {
        return selectedPerson = getPersonById(id);
    }

    public ArrayList<Rentable> getVacantRentables() {
        return rentables.stream()
                .filter(flat -> flat.getPayingPerson() == null)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<Rentable> getRentablesFromResidentials(ArrayList<Residential> residentials) {
        return Stream.concat(
                residentials.stream()
                        .flatMap(residential -> residential.getBlocksOfFlats().stream())
                        .flatMap(block -> block.getFlats().stream()),
                residentials.stream()
                        .flatMap(residential -> residential.getParkingSpaces().stream()))
                .collect(Collectors.toCollection(ArrayList::new));

    }

    public ArrayList<Rentable> getRentables() {
        return rentables;

    }

    public Optional<Rentable> selectRentableById(int id) {
        return selectedRentable = getRentableById(id);

    }

    public Optional<Rentable> getRentableById(int id) {
        return rentables.stream()
                .filter(r -> r.getId() == id)
                .findFirst();
    }

    public void rent() {
        if (selectedPerson.isPresent() && selectedRentable.isPresent()) {
            Person person = selectedPerson.get();
            Rentable rentable = selectedRentable.get();
            if (person.getRentedObjects().size() < 5) {
                if (person.getWarnings().size() < 3) {
                    if (rentable.rent(person)) {
                        Controller.rentableSuccessfullyRented();
                    }
                } else {
                    try {
                        Properties prop = Main.loadProperties();
                        StringBuilder builder = new StringBuilder();
                        builder.append(prop.getProperty("personEx"));
                        builder.append(person.getFirstname());
                        builder.append(" ");
                        builder.append(person.getLastname());
                        builder.append(prop.getProperty("alreadyRentedTheseRentablesEx"));
                        person.getRentedObjects().forEach(r -> builder.append(r.toString()));
                        builder.append(prop.getProperty("RED_BOLD"));
                        throw new ProblematicTenantException(builder.toString());
                    } catch (ProblematicTenantException ex) {
                        System.out.println(ex.getMessage());
                    }
                }

            } else {
                Controller.alreadyRentedAllPossibleRentables();
            }

        } else {
            Controller.personOrRentableNotSelected();
        }
    }

    public void cancelRent(){
        if (selectedPerson.isPresent() && selectedRentable.isPresent()) {
            Person person = selectedPerson.get();
            Rentable rentable = selectedRentable.get();
            if(person.getRentedObjects().contains(rentable)){
                Optional<File> warning = person.getWarnings().stream().filter(f -> f.getName().equals(String.valueOf(rentable.getId()))).findFirst();

                if(DAYS.between(rentable.getEndOfTheLease(), TimeChanger.getDate()) <= 30 && warning.isPresent()){
                    person.getWarnings().remove(warning.get());

                }
                rentable.setRentCancelled(true);
                Controller.rentWillBeCancelled();
            }else{
                Controller.youDontRentThisRentable();
            }
        } else {
            Controller.personOrRentableNotSelected();
        }
    }

    public void addInhabitant(Person inhabitant, Flat flat) {
        if (flat.getPayingPerson() != null) {
            flat.getInhabitants().add(inhabitant);
            Controller.operationWasSuccessfull();
        }
    }

    public void addStorable(Storable storable, ParkingSpace parkingSpace) {
        try {
            parkingSpace.addStorable(storable);
            Controller.operationWasSuccessfull();
        } catch (TooManyThingsException ex) {
            Controller.noMoreSpaceInParkingSpace(ex.getMessage());
        }
    }

    public void removeInhabitant(Person person, Flat flat) {
        if (flat.getPayingPerson().equals(person)) {
            flat.setPayingPerson(null);
            flat.getInhabitants().clear();
        } else {
            flat.getInhabitants().remove(person);
        }
    }


    public Optional<Person> getSelectedPerson() {
        return selectedPerson;
    }

    public Optional<Rentable> getSelectedRentable() {
        return selectedRentable;
    }


    public Optional<Person> getPersonById(int id) {
        return people.stream()
                .filter(p -> p.getId() == id)
                .findFirst();
    }

    public ArrayList<Rentable> getSelectedPersonRentables() {
        if (selectedPerson.isPresent())
            return selectedPerson.get().getRentedObjects();
        else return new ArrayList<>();
    }


    public void renewRent() {
        if (selectedPerson.isPresent() && selectedRentable.isPresent()) {
            if (selectedPerson.get().getRentedObjects().contains(selectedRentable.get())) {
                selectedPerson.get().getWarnings()
                        .removeIf(w -> w.getName()
                                .equals(String.valueOf(selectedRentable.get().getId())));
                selectedRentable.get().setEndOfTheLease(selectedRentable.get().getEndOfTheLease().plusMonths(1));
                Controller.operationWasSuccessfull();
            } else {
                Controller.youDontRentThisRentable();
            }
        } else {
            Controller.personOrRentableNotSelected();
        }

    }

    public ArrayList<Storable> getAvailableStorables() {
        return storables.stream()
                .filter(s -> !s.isStored())
                .collect(Collectors.toCollection(ArrayList::new));
    }


    public void generateRaport() throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append("Raport osiedla\n");
        builder.append("Pomieszczenia:\n\n");

        rentables.stream()
                .sorted(Comparator.comparing(Rentable::getUsableArea))
                .forEach(r -> builder.append(r.getRaportData()));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("--yyyy-MM-dd--HH-mm-ss");

        String path = "././raporty/Raport Osiedla" + LocalDateTime.now().format(formatter) + ".txt";


        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(builder.toString());
        writer.close();
    }


}
